package com.example.paseadog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    TextInputLayout tilEmail;
    TextInputEditText etEmail;
    TextInputLayout tilPassword;
    TextInputEditText etPassword;

    AppCompatButton btnRegister;
    AppCompatButton btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);


        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(v -> onRegisterClick());

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> onLoginClick());


    }

    private void onRegisterClick() {

        Intent intent = new Intent(LoginActivity.this, RegistroUsuario.class);
        startActivity(intent);

    }

    private void onLoginClick() {
        String email = etEmail.getText().toString();
        if (email.isEmpty()) {
            tilEmail.setError("Email es obligatorio");
        } else {

            Intent intent = new Intent(LoginActivity.this, PaginaPrincipal.class);
            startActivity(intent);

        }
    }
}