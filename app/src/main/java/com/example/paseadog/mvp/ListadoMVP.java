package com.example.paseadog.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

import com.example.paseadog.model.ListadoInteractor;

public interface ListadoMVP {

    interface Model {
        void loadListado(LoadListadoCallback callback);
        void getUserInfo(ListadoInteractor.UserInfoCallback callback);

        interface LoadListadoCallback{
            void setListado(List<ListadoDto> listado);
        }




    }
    interface Presenter {
        void loadListado();

        void onListadoClick();

        void onNewSaleClick();

        void onSelectItem(ListadoDto item);

        void getUserInfo();
    }

    interface View {
        Activity getActivity();

        void showProgressBar();

        void hideProgressBar();

        void showListado(List<ListadoDto>listado);

        void openDetailsActivity(Bundle params);

        void showUserInfo(UserInfo userInfo);

        void openLocationActivity(Bundle params);
    }

    class ListadoDto {
        private String client;
        private String address;

        public ListadoDto(String client, String address) {
            this.client = client;
            this.address = address;
        }

        public String getClient() {
            return client;
        }

        public String getAddress() {
            return address;
        }
    }

    public class UserInfo {
        private String name;
        private String email;
        private Boolean enable;

        public UserInfo(String name, String email, Boolean enable) {
            this.name = name;
            this.email = email;
            this.enable = enable;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public Boolean getEnable() {
            return enable;
        }
    }
}
