package com.example.paseadog.mvp;

import android.app.Activity;
import android.content.Intent;

import java.util.List;

public interface RegistroUsuarioMVP {

    interface Model{


        void getAllUsers(RegistroUsuarioMVP.Model.GetUserCallback<List<RegistroUsuarioMVP.UserInfo>> callback);
        boolean hasAuthenticatedUser();



        void validateCredentials(String email, String password,
                                 ValidateCredentialsCallback callback);

        Intent getGoogleIntent();

        void setGoogleData(Intent data, ValidateCredentialsCallback callback);


        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }

        interface GetUserCallback<T> {
            void onSuccess(T data);

            void onFailure();
        }

    }

    interface Presenter{

        void onRegister2Click();
    }

    interface View{

        RegistroUsuarioInfo getRegistroInfo();
        void showNameError(String error);
        void showLastNameError(String error);
        void showEmailError(String error);
        void showPhoneError(String error);
        void showIdError(String error);
        void showPasswordError(String error);
        void showPaginaPrincipal();
        void registrarUsuarios();

        Activity getActivity();

        void showProgresBar();

        void showListadoActivity();

        void hideProgresBar();

        void showGeneralError(String credenciales_inválidas);
    }
    class RegistroUsuarioInfo {
        private  String name;
        private String  lastname;
        private  String email;
        private String password;
        private  String phone;
        private  String docu;

        public RegistroUsuarioInfo(String name, String lastname, String email,String password, String phone, String docu) {
            this.name = name;
            this.lastname = lastname;
            this.email = email;
            this.password = password;
            this.phone = phone;
            this.docu = docu;
        }

        public String getName() {
            return name;
        }

        public String getLastname() {
            return lastname;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() { return password; }

        public String getPhone() {
            return phone;
        }

        public String getId() {
            return docu;
        }
    }

    class UserInfo {
        private String name;
        private String lastname;
        private String email;
        private String password;
        private String phone;
        private String docu;

        public UserInfo(String name, String lastname, String email, String password, String phone, String docu) {
            this.name = name;
            this.lastname = lastname;
            this.email = email;
            this.password = password;
            this.phone = phone;
            this.docu = docu;
        }

        public String getName() {
            return name;
        }

        public String getLastname() {
            return lastname;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() { return password; }

        public String getPhone() {
            return phone;
        }

        public String getId() {
            return docu;
        }

        @Override
        public String toString() {
            return "UserInfo {" +
                    "name='" + name + '\'' +
                    "lastname='" + lastname + '\'' +
                    ", email='" + email + '\'' +
                    "password='" + password + '\'' +
                    "phone='" + phone + '\'' +
                    "docu='" + docu + '\'' +
                    '}';
        }
    }
}
