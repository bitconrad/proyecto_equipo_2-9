package com.example.paseadog.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.example.paseadog.R;

public class PaginaPrincipal extends AppCompatActivity {

    AppCompatButton btnRegister3;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagina_principal);

        btnRegister3 = findViewById(R.id.button5);
        btnRegister3.setOnClickListener(v -> onRegister3Click());



    }

    private void onRegister3Click() {

        Intent intent = new Intent( PaginaPrincipal.this, RegistroPaseador.class);
        startActivity(intent);

    }
}