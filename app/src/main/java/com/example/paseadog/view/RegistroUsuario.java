package com.example.paseadog.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.paseadog.R;
import com.example.paseadog.model.RegistroUsuarioInteractor;
import com.example.paseadog.mvp.RegistroUsuarioMVP;
import com.example.paseadog.presenter.LoginPresenter;
import com.example.paseadog.presenter.RegistroUsuarioPresenter;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistroUsuario extends AppCompatActivity implements RegistroUsuarioMVP.View {

    private TextInputLayout tilName;
    private TextInputEditText etName;

    private TextInputLayout tilLastName;
    private TextInputEditText etLastName;

    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;

    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;

    private TextInputLayout tilPhone;
    private TextInputEditText etPhone;

    private TextInputLayout tildocu;
    private TextInputEditText etdocu;

    private CircularProgressIndicator cpiWait;

    private AppCompatButton btnRegister2;

    private ProgressDialog progressDialog;

    private RegistroUsuarioMVP.Presenter presenter;

    private DatabaseReference Usuarios;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        presenter = new RegistroUsuarioPresenter(RegistroUsuario.this);

        initUI();
    }

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);
        Usuarios = FirebaseDatabase.getInstance().getReference();
        presenter = new RegistroUsuarioPresenter(this);

        initUI();

    }*/

    private void initUI() {

        cpiWait = findViewById(R.id.cpi_wait);

        tilName = findViewById(R.id.til_name);
        etName = findViewById(R.id.et_name);

        tilLastName = findViewById(R.id.til_lastname);
        etLastName = findViewById(R.id.et_lastname);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        tilPhone = findViewById(R.id.til_phone);
        etPhone = findViewById(R.id.et_phone);

        tildocu = findViewById(R.id.til_docu);
        etdocu = findViewById(R.id.et_docu);

        btnRegister2 = findViewById(R.id.btn_register);
        btnRegister2.setOnClickListener(v -> presenter.onRegister2Click());
        progressDialog = new ProgressDialog(this);

    }


    @Override
    public RegistroUsuarioMVP.RegistroUsuarioInfo getRegistroInfo() {
        return new RegistroUsuarioMVP.RegistroUsuarioInfo(
                etName.getText().toString().trim(),
                etLastName.getText().toString().trim(),
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim(),
                etPhone.getText().toString().trim(),
                etdocu.getText().toString().trim());

    }



    @Override
    public void showNameError(String error) {
        tilName.setError(error);
    }

    @Override
    public void showLastNameError(String error) {
        tilLastName.setError(error);

    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);

    }

    @Override
    public void showPhoneError(String error) {
        tilPhone.setError(error);

    }

    @Override
    public void showIdError(String error) {
        tildocu.setError(error);

    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showPaginaPrincipal() {
        Intent intent = new Intent( RegistroUsuario.this, PaginaPrincipal.class );
        startActivity(intent);
    }

    @Override
    public void registrarUsuarios() {

    }

    @Override
    public Activity getActivity() {
        return null;
    }

    @Override
    public void showProgresBar() {

    }

    @Override
    public void showListadoActivity() {
        Intent intent = new Intent(RegistroUsuario.this, ListadoActivity.class);
        startActivity(intent);
    }

    @Override
    public void hideProgresBar() {

    }

    @Override
    public void showGeneralError(String credenciales_inválidas) {

    }


    /*public void registrarUsuarios(){
        String name = etName.getText().toString();
        String lastname = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String phone = etPhone.getText().toString();
        String docu = etdocu.getText().toString();

        if (!TextUtils.isEmpty(name)){

            String id = Usuarios.push().getKey();
            RegistroUsuarioInteractor.Usuarios Users = new RegistroUsuarioInteractor.Usuarios(id, name, lastname, email, password, phone, docu);
            Usuarios.child("users").child(id).setValue(Users);
            Toast.makeText(this,"Usuario registrado", Toast.LENGTH_LONG).show();
        }

    }*/




}
