package com.example.paseadog.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import com.example.paseadog.R;
import com.example.paseadog.mvp.ListadoMVP;

public class ListadoAdapter extends RecyclerView.Adapter<ListadoAdapter.ViewHolder> {

    private List<ListadoMVP.ListadoDto> data;
    private OnItemClickListener listener;

    public ListadoAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<ListadoMVP.ListadoDto> data) {
        this.data = data;
        //notifyDataSetChanged();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_listado, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ListadoMVP.ListadoDto item = data.get(position);

        if (listener != null) {
            holder.itemView.setOnClickListener(v -> {
                listener.onItemClicked(item);
            });
        }

        holder.getTvName().setText(item.getClient());
        holder.getTvAddress().setText(item.getAddress());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivClient;
        private TextView tvName;
        private TextView tvAddress;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            initUI(view);
        }

        private void initUI(View view) {
            ivClient = view.findViewById(R.id.iv_client);
            tvName = view.findViewById(R.id.tv_name);
            tvAddress = view.findViewById(R.id.tv_address);
        }

        public ImageView getIvClient() {
            return ivClient;
        }

        public TextView getTvName() {
            return tvName;
        }

        public TextView getTvAddress() {
            return tvAddress;
        }
    }

    public static interface OnItemClickListener {
        void onItemClicked(ListadoMVP.ListadoDto item);
    }
}
