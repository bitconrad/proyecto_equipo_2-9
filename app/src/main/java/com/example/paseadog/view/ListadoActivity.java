package com.example.paseadog.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseUser;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

import com.example.paseadog.R;
import com.example.paseadog.model.repository.FirebaseAuthRepository;
import com.example.paseadog.mvp.ListadoMVP;
import com.example.paseadog.presenter.ListadoPresenter;
import com.example.paseadog.view.adapters.ListadoAdapter;

public class ListadoActivity extends AppCompatActivity implements ListadoMVP.View {

    private LinearProgressIndicator pbWait;

    private DrawerLayout drawerLayout;
    private MaterialToolbar toolbar;
    private NavigationView navigationView;

    private TextView tvClient;
    private TextView tvEmail;

    private RecyclerView rvListado;
    private FloatingActionButton btnNewSale;

    private ListadoMVP.Presenter presenter;
    private ListadoAdapter listadoAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);

        presenter = new ListadoPresenter(ListadoActivity.this);

        initUI();

        loadData();
    }

    private void loadData() {
        presenter.loadListado();


        presenter.getUserInfo();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.app_name)
                .setMessage("Desea cerrar sesión?")
                .setPositiveButton("Si",
                        (dialog, which) -> {
                            FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(ListadoActivity.this);
                            repository.logOut();

                            ListadoActivity.super.onBackPressed();
                        })
                .setNegativeButton("No", null);

        builder.create().show();
    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        toolbar = findViewById(R.id.app_toolbar);
        toolbar.setNavigationOnClickListener(v -> drawerLayout.openDrawer(navigationView));

        pbWait = findViewById(R.id.pb_wait);

        navigationView = findViewById(R.id.nv_listado);
        navigationView.setNavigationItemSelectedListener(this::onMenuItemClick);

        rvListado = findViewById(R.id.rv_listado);
        rvListado.setLayoutManager(new LinearLayoutManager(ListadoActivity.this));
        listadoAdapter = new ListadoAdapter();
        listadoAdapter.setListener(item -> presenter.onSelectItem(item));
        rvListado.setAdapter(listadoAdapter);


        btnNewSale = findViewById(R.id.btn_new_sale);
        btnNewSale.setOnClickListener(v -> onNewSaleClick());

        tvClient = navigationView.getHeaderView(0).findViewById(R.id.tv_client);
        tvEmail = navigationView.getHeaderView(0).findViewById(R.id.tv_email);
    }

    private boolean onMenuItemClick(MenuItem menuItem) {
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
        return true;
    }

    private void onNewSaleClick() {
        startActivity(new Intent(ListadoActivity.this, NewSaleActivity.class));
    }

    @Override
    public Activity getActivity() {
        return ListadoActivity.this;
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);
    }

    @Override
    public void showListado(List<ListadoMVP.ListadoDto> listado) {
        listadoAdapter.setData(listado);
    }

    @Override
    public void openDetailsActivity(Bundle params) {
        Intent intent = new Intent(ListadoActivity.this, NewSaleActivity.class);
        //intent.putExtras(params);
        startActivity(intent);
    }

    @Override
    public void showUserInfo(ListadoMVP.UserInfo userInfo) {
        tvClient.setText(userInfo.getName());
        tvEmail.setText(userInfo.getEmail());

    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(ListadoActivity.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);
    }

}


