package com.example.paseadog.presenter;

import android.text.TextUtils;
import android.widget.Toast;
import android.content.Intent;

import android.util.Log;
import android.widget.Toast;

import java.util.List;

import com.example.paseadog.model.RegistroUsuarioInteractor;
import com.example.paseadog.mvp.LoginMVP;
import com.example.paseadog.mvp.RegistroUsuarioMVP;



public class RegistroUsuarioPresenter implements RegistroUsuarioMVP.Presenter {

    private RegistroUsuarioMVP.View view;
    private  RegistroUsuarioMVP.Model model;

    public RegistroUsuarioPresenter(RegistroUsuarioMVP.View view){
        this.view = view;
        this.model = new RegistroUsuarioInteractor(view.getActivity());
    }



    @Override
    public void onRegister2Click() {


        boolean error = false;
        RegistroUsuarioMVP.RegistroUsuarioInfo RegistroUsuarInfo = view.getRegistroInfo();

        view.showNameError("");
        view.showLastNameError("");
        view.showEmailError("");
        view.showPasswordError("");
        view.showPhoneError("");
        view.showIdError("");

        if (RegistroUsuarInfo.getName().isEmpty()){
            view.showNameError("Nombre es obligatorio");
            error = true;
        } else if(!isNameValid(RegistroUsuarInfo.getName())){
            view.showNameError("Nombre no valido");
            error = true;
        }
        if (RegistroUsuarInfo.getLastname().isEmpty()){
            view.showLastNameError("Apellido es obligatorio");
            error = true;
        } else if(!isLastNameValid(RegistroUsuarInfo.getLastname())){
            view.showLastNameError("Apellido no valido");
            error = true;
        }
        if (RegistroUsuarInfo.getEmail().isEmpty()){
            view.showEmailError("Correo es obligatorio");
            error = true;
        } else if(!isEmailValid(RegistroUsuarInfo.getEmail())){
            view.showEmailError("Correo no valido");
            error = true;
        }
        if (RegistroUsuarInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        } else if (!isPasswordValid(RegistroUsuarInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios de seguridad");
            error = true;
            }

        if (RegistroUsuarInfo.getPhone().isEmpty()){
            view.showPhoneError("Celular es obligatorio");
            error = true;
        } else if(!isPhoneValid(RegistroUsuarInfo.getPhone())){
            view.showPhoneError("Numero celular no valido");
            error = true;
        }
        if (RegistroUsuarInfo.getId().isEmpty()){
            view.showIdError("Documento de identificacion es obligatorio");
            error = true;
        } else if (!isIdValid(RegistroUsuarInfo.getId())){
            view.showIdError("Documento identificacion no valido");
            error = true;
        }
        if(!error) {
            //view.registrarUsuarios();
            //view.showPaginaPrincipal();
            //Toast.makeText(this, "Registrado", Toast.LENGTH_LONG).show();

            view.showListadoActivity();

            //Toast.makeText(this, "Registrado!", Toast.LENGTH_LONG).show();









        }
    }


    private boolean isIdValid(String id) {
        return id.length() >8 && id.length() <=10;
    }

    private boolean isPhoneValid(String phone) {
        return phone.length() > 6 && phone.length() <= 13;
    }

    private boolean isLastNameValid(String lastname) {
        return lastname.matches("(?i)[a-z]([- ',.a-z]{0,23}[a-z])?");
    }

    private boolean isNameValid(String name) {
        return name.matches("(?i)[a-z]([- ',.a-z]{0,23}[a-z])?");
    }


    private boolean isEmailValid(String email) {
        return email.contains("@") && email.endsWith(".com");
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

}
