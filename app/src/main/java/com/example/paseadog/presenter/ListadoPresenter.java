package com.example.paseadog.presenter;

import android.os.Bundle;

import java.util.List;

import com.example.paseadog.model.ListadoInteractor;
import com.example.paseadog.mvp.ListadoMVP;

public class ListadoPresenter implements ListadoMVP.Presenter {

    private ListadoMVP.View view;
    private ListadoMVP.Model model;

    public ListadoPresenter(ListadoMVP.View view) {
        this.view = view;
        this.model = new ListadoInteractor(view.getActivity());
    }

    @Override
    public void loadListado() {
        view.showProgressBar();
        new Thread(() -> model.loadListado(new ListadoMVP.Model.LoadListadoCallback() {
            @Override
            public void setListado(List<ListadoMVP.ListadoDto> listado) {
                view.getActivity().runOnUiThread(() -> {
                    view.hideProgressBar();
                    view.showListado(listado);
                });
            }
        })).start();
    }

    @Override
    public void onListadoClick() {

    }

    @Override
    public void onNewSaleClick() {

    }

    @Override
    public void onSelectItem(ListadoMVP.ListadoDto item) {
        Bundle params = new Bundle();
        params.putString("name", item.getClient());
        params.putString("address", item.getAddress());

        view.openLocationActivity(params);
    }

    @Override
    public void getUserInfo(){
        new Thread(() ->{

            model.getUserInfo(userInfo -> {
                view.getActivity().runOnUiThread(() -> {
                    view.showUserInfo(userInfo);
                });
            });

        }).start();


    }
}


