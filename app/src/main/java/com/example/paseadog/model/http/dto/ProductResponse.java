package com.example.paseadog.model.http.dto;

/*
{
        "_id": "61531dbfecf353811724208d",
        "code": "1001",
        "name": "Lapicero azul",
        "price": 1200,
        "categories": [
            "azul",
            "papeleria",
            "tinta"
        ],
        "__v": 0
    },

 */

import java.util.List;

public class ProductResponse {
    private String code;
    private String name;
    private Integer price;
    private List<String> categories;
    private String _id;
    private String __v;
    private String imageUrl;

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "ProductResponse{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", categories=" + categories +
                '}';
    }
}
