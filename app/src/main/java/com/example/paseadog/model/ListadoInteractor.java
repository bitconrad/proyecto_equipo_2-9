package com.example.paseadog.model;

import android.content.Context;

import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.paseadog.model.database.entity.User;
import com.example.paseadog.model.repository.FirebaseAuthRepository;
import com.example.paseadog.model.repository.UserRepository;
import com.example.paseadog.mvp.ListadoMVP;

import javax.security.auth.callback.Callback;

public class ListadoInteractor implements ListadoMVP.Model {

    private List<ListadoMVP.ListadoDto> listado;

    private UserRepository userRepository;
    private FirebaseAuthRepository firebaseAuthRepository;

    public ListadoInteractor(Context context) {
        userRepository = new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);

        listado = Arrays.asList(
                new ListadoMVP.ListadoDto("santiago cortazar", "Calle 8 # 9 - 10, Libano, Colombia"),
                new ListadoMVP.ListadoDto("freddie mendez", "Calle 2 # 15 - 23, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("maria gomez", "Calle 3 # 2 - 55, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("alejandro florez", "Cra 21 # 3 - 33, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("bernardo cardona", "Cra 22 # 7 - 56, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("santiago cortazar", "Calle 8 # 9 - 10, Libano, Colombia"),
                new ListadoMVP.ListadoDto("freddie mendez", "Calle 2 # 15 - 23, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("maria gomez", "Calle 3 # 2 - 55, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("alejandro florez", "Cra 21 # 3 - 33, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("bernardo cardona", "Cra 22 # 7 - 56, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("santiago cortazar", "Calle 8 # 9 - 10, Libano, Colombia"),
                new ListadoMVP.ListadoDto("freddie mendez", "Calle 2 # 15 - 23, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("maria gomez", "Calle 3 # 2 - 55, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("alejandro florez", "Cra 21 # 3 - 33, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("bernardo cardona", "Cra 22 # 7 - 56, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("santiago cortazar", "Calle 8 # 9 - 10, Libano, Colombia"),
                new ListadoMVP.ListadoDto("freddie mendez", "Calle 2 # 15 - 23, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("maria gomez", "Calle 3 # 2 - 55, Bogota, Colombia"),
                new ListadoMVP.ListadoDto("alejandro florez", "Cra 21 # 3 - 33, Bogota, Colombia")
        );

    }

    @Override
    public void loadListado(LoadListadoCallback callback) {
        callback.setListado(listado);
    }



    @Override
    public void getUserInfo(UserInfoCallback callback) {
       if(firebaseAuthRepository.isAuthenticated()) {
            String uid = firebaseAuthRepository.getCurrentUser().getUid();//se va a Firebase y le pide el UID
            userRepository.getUserByUID(uid, new UserRepository.UserCallback<User>() {
                @Override
                public void onSuccess(User user) {
                    ListadoMVP.UserInfo userInfo = new ListadoMVP.UserInfo(user.getName(),
                            user.getEmail(), user.getEnable());
                    callback.onSuccess(userInfo);
                }

                @Override
                public void onFailure() {
                    // No hace nada
                }
            });
        }
    }

    public interface UserInfoCallback {
        void onSuccess(ListadoMVP.UserInfo userInfo);
    }
}
