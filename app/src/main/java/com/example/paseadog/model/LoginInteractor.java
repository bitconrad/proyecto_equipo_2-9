package com.example.paseadog.model;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import com.example.paseadog.model.database.entity.User;
import com.example.paseadog.model.repository.FirebaseAuthRepository;
import com.example.paseadog.model.repository.ProductRepository;
import com.example.paseadog.model.repository.UserRepository;
import com.example.paseadog.mvp.LoginMVP;

public class LoginInteractor implements LoginMVP.Model {

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final FirebaseAuthRepository firebaseAuthRepository;

    public LoginInteractor(Context context) {
        userRepository = new UserRepository(context);
        productRepository = new ProductRepository();
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
    }
//PARA CREAR USUARIO EN REAL TIME DATABASE
    public void createUser(User user) {
        firebaseAuthRepository.createUser(user.getEmail(), user.getPassword(),
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        user.setUid(firebaseAuthRepository.getCurrentUser().getUid());
                        userRepository.createUser(user, new UserRepository.UserCallback<Void>() {
                            @Override
                            public void onSuccess(Void node) {
                                // Enviar confirmacion a presenter
                            }

                            @Override
                            public void onFailure() {
                                // Enviar error a presenter
                            }
                        });
                    }

                    @Override
                    public void onFailure() {
                        // Enviar error a presenter
                    }
                });
    }
//SIRVE PARA CREAR USUARIO EN BD AUTH
    @Override
    public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback) {

        firebaseAuthRepository.validateUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        callback.onFailure();
                    }
                });

        /*
        userRepository.getUserByEmail(email, new UserRepository.UserCallback<User>() {
            @Override
            public void onSuccess(User user) {
                if (user != null && user.getPassword().equals(password)) {
                    callback.onSuccess();
                } else {
                    callback.onFailure();
                }
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });

        productRepository.getProductByCode("10010",
                new ProductRepository.ProductCallback<ProductResponse>() {
                    @Override
                    public void onSuccess(ProductResponse dato) {
                        Log.i(LoginInteractor.class.getSimpleName() + "-ByCode",
                                dato.toString());
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e(LoginInteractor.class.getSimpleName() + "-ByCode", error);
                    }
                });

        productRepository.getAll(
                new ProductRepository.ProductCallback<List<ProductResponse>>() {
                    @Override
                    public void onSuccess(List<ProductResponse> dato) {
                        for (ProductResponse product : dato) {
                            Log.i(LoginInteractor.class.getSimpleName() + "-All",
                                    product.toString());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e(LoginInteractor.class.getSimpleName() + "-All", error);
                    }
                });
        */
    }

    @Override
    public void getAllUsers(GetUserCallback<List<LoginMVP.UserInfo>> callback) {
        userRepository.getAllUsers(new UserRepository.UserCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> values) {
                List<LoginMVP.UserInfo> users = new ArrayList<>();
                for (User user : values) {
                    if (user.getEnable() == null || !user.getEnable()) {
                        continue;
                    }

                    users.add(new LoginMVP.UserInfo(user.getName(), user.getEmail()));
                }
                callback.onSuccess(users);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

    @Override
    public boolean hasAuthenticatedUser() {

        return firebaseAuthRepository.isAuthenticated();
    }

    @Override
    public Intent getGoogleIntent() {

        return firebaseAuthRepository.getGoogleSingInIntent();
    }

    @Override
    public void setGoogleData(Intent data, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.setGoogleData(data,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {

                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {

                        callback.onFailure();
                    }
                });
    }

}

