package com.example.paseadog.model.repository;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import com.example.paseadog.model.http.ProductApi;
import com.example.paseadog.model.http.RetrofitHelper;
import com.example.paseadog.model.http.dto.ProductResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductRepository {
    private ProductApi productApi;

    public ProductRepository() {
        productApi = RetrofitHelper.getProductApi();
    }

    public void getAll(ProductCallback<List<ProductResponse>> callback) {
        productApi.getAll()
                .enqueue(new Callback<List<ProductResponse>>() {
                    @Override
                    public void onResponse(Call<List<ProductResponse>> call, Response<List<ProductResponse>> response) {
                        if (response.isSuccessful()) {
                            callback.onSuccess(response.body());
                        } else {
                            try {
                                callback.onFailure(response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ProductResponse>> call, Throwable t) {
                        callback.onFailure(t.getMessage());
                    }
                });
    }

    public void getProductByCode(String code, ProductCallback<ProductResponse> callback) {
        productApi.getByCode(code)
                .enqueue(new Callback<ProductResponse>() {
                    @Override
                    public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                        if (response.isSuccessful()) {
                            callback.onSuccess(response.body());
                        } else {
                            try {
                                callback.onFailure(response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductResponse> call, Throwable t) {
                        callback.onFailure(t.getMessage());
                    }
                });
    }

    public interface ProductCallback<T> {
        void onSuccess(T dato);

        void onFailure(String error);
    }
}
