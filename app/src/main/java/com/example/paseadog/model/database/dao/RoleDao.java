package com.example.paseadog.model.database.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import com.example.paseadog.model.database.entity.Role;

@Dao
public interface RoleDao {

    @Query("SELECT * FROM role")
    List<Role> getAll();
}
