package com.example.paseadog.model.repository;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import com.example.paseadog.R;

public class FirebaseAuthRepository {
    private static FirebaseAuthRepository instance;

    public static FirebaseAuthRepository getInstance(Context context) {
        if (instance == null) {
            instance = new FirebaseAuthRepository(context);
        }
        return instance;
    }

    private final FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    private final GoogleSignInClient googleSignInClient;

    private FirebaseAuthRepository(Context context) {
        mAuth = FirebaseAuth.getInstance();

        // Iniciar cliente para autenticar con google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(context, gso);

    }

    public boolean isAuthenticated() {
        return getCurrentUser() != null;
    }

    public FirebaseUser getCurrentUser() {
        if (currentUser == null) {
            currentUser = mAuth.getCurrentUser();
        }
        return currentUser;
    }

    public void logOut() {
        if (currentUser != null) {
            mAuth.signOut();
            currentUser = null;
        }
    }

    public void createUser(String email, String password, FirebaseAuthCallback callback) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public void validateUser(String email, String password, FirebaseAuthCallback callback) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public interface FirebaseAuthCallback {
        void onSuccess();

        void onFailure();
    }

    // Google Auth
    public Intent getGoogleSingInIntent() {
        return googleSignInClient.getSignInIntent();
    }

    public void setGoogleData(Intent data, FirebaseAuthCallback callback) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            firebaseAuthWithGoogle(account.getIdToken(), callback);
        } catch (ApiException e) {
            callback.onFailure();
        }
    }

    private void firebaseAuthWithGoogle(String idToken, FirebaseAuthCallback callback) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }
}
