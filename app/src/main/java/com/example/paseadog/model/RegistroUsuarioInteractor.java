package com.example.paseadog.model;

import android.content.Context;
import android.content.Intent;

import java.util.List;

import com.example.paseadog.model.repository.FirebaseAuthRepository;
import com.example.paseadog.mvp.LoginMVP;
import com.example.paseadog.mvp.RegistroUsuarioMVP;

public class RegistroUsuarioInteractor implements RegistroUsuarioMVP.Model {

    private  final FirebaseAuthRepository firebaseAuthRepository;

    public RegistroUsuarioInteractor(Context context){
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context); //me da una instancia del obj

    }


    @Override
    public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback){
        firebaseAuthRepository.createUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onFailure() {

                    }
                });
    }

    @Override
    public void getAllUsers(RegistroUsuarioMVP.Model.GetUserCallback<List<RegistroUsuarioMVP.UserInfo>> callback) {

    }

    @Override
    public boolean hasAuthenticatedUser() {
        return false;
    }

    @Override
    public Intent getGoogleIntent() {
        return null;
    }

    @Override
    public void setGoogleData(Intent data, RegistroUsuarioMVP.Model.ValidateCredentialsCallback callback) {

    }

    /*public static class Usuarios {
        String id, name, lastname, email, password, phone, docu;
        public Usuarios(String id, String name, String lastname, String email, String password, String phone, String docu) {
            this.id = id;
            this.name = name;
            this.lastname = lastname;
            this.email = email;
            this.password = password;
            this.phone = phone;
            this.docu = docu;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getLastname() {
            return lastname;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public String getPhone() {
            return phone;
        }

        public String getDocu() {
            return docu;
        }
    }*/
}
