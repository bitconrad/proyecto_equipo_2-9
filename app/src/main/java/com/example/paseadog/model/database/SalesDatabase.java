package com.example.paseadog.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.paseadog.model.database.dao.RoleDao;
import com.example.paseadog.model.database.dao.UserDao;
import com.example.paseadog.model.database.entity.Role;
import com.example.paseadog.model.database.entity.User;

@Database(entities = {User.class, Role.class}, version = 1)
public abstract class SalesDatabase extends RoomDatabase {
    private volatile static SalesDatabase instance;

    public static SalesDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .databaseBuilder(context.getApplicationContext(), SalesDatabase.class, "sales-database")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract UserDao getUserDao();

    public abstract RoleDao getRoleDao();
}
