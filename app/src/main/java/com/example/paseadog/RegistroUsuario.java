package com.example.paseadog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

public class RegistroUsuario extends AppCompatActivity {

    AppCompatButton btnRegister2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        btnRegister2 = findViewById(R.id.btn_register);
        btnRegister2.setOnClickListener(v -> onRegister2Click());



    }

    private void onRegister2Click() {

        Intent intent = new Intent(RegistroUsuario.this, PaginaPrincipal.class);
        startActivity(intent);

    }
}